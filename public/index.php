<?php
error_reporting(E_ALL);
set_time_limit(9999);

require 'ncsparser/NCSParser.php';
require 'ncsparser/ArrayToCSV.php';

// parse
$NCSParser = new NCSParser;
$NCSParser->limit_letters = false;
$NCSParser->limit_letter_profiles = false;
$data = $NCSParser->parse();
// download
$headers = [
    'Role title',
    'Alternative titles',
    '1 Sentence overview',
    'Salary low',
    'Salary high',
    'Working hours low',
    'Working hours high',
    'Entry requirements',
    'Skills required',
    'What you\'ll do',
    'Salary',
    'Working hours, patterns and environment',
    'Career path and progression',
];
$filename = 'ncs_data_' . date('d.m.Y_H:i') . '.csv';
$ArrayToCSV = new ArrayToCSV;
$ArrayToCSV->downloadCSV($filename, $headers, $data);