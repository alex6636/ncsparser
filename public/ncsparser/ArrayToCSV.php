<?php
class ArrayToCSV
{
    const CSV_DELIMITER = ',';

    public function downloadCSV($filename, array $headers, array $arr) {
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen('php://output', 'w');
        // put
        fputcsv($output, array_values($this->encode($headers)), static::CSV_DELIMITER);
        foreach ($arr as $a) {
            fputcsv($output, array_values($this->encode($a)), static::CSV_DELIMITER);
        }
        fclose($output);
    }

    protected function encode(array $arr) {
        return array_map(function($val) {
            return iconv("UTF-8", "Windows-1252", $val);
        }, $arr);

    }
}