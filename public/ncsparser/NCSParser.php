<?php
class NCSParser
{
    /** @var int|false $limit_letters */
    public $limit_letters = false;

    /** @var int|false $limit_letter_profiles */
    public $limit_letter_profiles = false;

    protected $site_url = 'https://nationalcareersservice.direct.gov.uk';
    protected $home_url = 'https://nationalcareersservice.direct.gov.uk/job-profiles/home';

    public function parse() {
        $result = [];
        $letters_urls = $this->parseLettersURLs();
        if (!empty($letters_urls)) {
            foreach ($letters_urls as $letter_url) {
                $profiles_urls = $this->parseProfilesURLs($letter_url);
                if (!empty($profiles_urls)) {
                    foreach ($profiles_urls as $profile_url) {
                        if ($parsed_profile = $this->parseProfile($profile_url)) {
                            $result[] = $parsed_profile;
                        }
                    }
                }
            }
        }
        return $result;
    }

    protected function parseLettersURLs() {
        if (!$home_contents = $this->getContents($this->home_url)) {
            return false;
        }
        $urls = [];
        // letters container
        preg_match('/<ul id=\"az\-index\">(.*)<\/ul>/sUi', $home_contents, $matches);
        if (!empty($matches[1])) {
            preg_match_all('/<a href=\"(.*)\">.*<\/a>/', $matches[1], $links_matches);
            if (!empty($links_matches[1])) {
                $letter_num = 0;
                foreach ($links_matches[1] as $href) {
                    $urls[] = $this->site_url . $href;
                    if (++ $letter_num === $this->limit_letters) {
                        break;
                    }
                }
            }
        }
        return $urls;
    }

    protected function parseProfilesURLs($letter_url) {
        if (!$letter_contents = $this->getContents($letter_url)) {
            return false;
        }
        $urls = [];
        preg_match_all('/<div class=\"column\-one\-half\">(.*)<\/div>/sUi', $letter_contents, $columns_matches);
        if (!empty($columns_matches[1])) {
            foreach ($columns_matches[1] as $column_contents) {
                preg_match_all('/<a href=\"(.*)\">.*<\/a>/', $column_contents, $links_matches);
                if (!empty($links_matches[1])) {
                    $profile_num = 0;
                    foreach ($links_matches[1] as $href) {
                        $urls[] = $this->site_url . $href;
                        if (++ $profile_num === $this->limit_letter_profiles) {
                            break 2;
                        }
                    }
                }
            }
        }
        return $urls;
    }

    protected function parseProfile($profile_url) {
        if (!$contents = $this->getContents($profile_url)) {
            return false;
        }
        $result = [
            'role_title'     =>NULL,
            'alt_titles'     =>NULL,
            'sent_overview'  =>NULL,
            'salary_low'     =>NULL,
            'salary_high'    =>NULL,
            'work_hours_low' =>NULL,
            'work_hours_high'=>NULL,
            'entry'          =>NULL,
            'skills'         =>NULL,
            'what_do'        =>NULL,
            'salary'         =>NULL,
            'work_hours'     =>NULL,
            'career'         =>NULL,
        ];
        // titles
        preg_match('/<h1 class="heading-xlarge">(.*)<\/h1>/sUi', $contents, $heading_matches);
        if (!empty($heading_matches[1])) {
            // secondary title
            preg_match('/<span class=\"heading\-secondary\">(.*)<\/span>/sUi', $heading_matches[1], $sec_heading_matches);
            if (!empty($sec_heading_matches)) {
                $result['role_title'] = trim(str_replace($sec_heading_matches[0], '', $heading_matches[1]));
                $result['alt_titles'] = trim($sec_heading_matches[1]);
            } else {
                $result['role_title'] = trim($heading_matches[1]);
            }
        }
        // sentence overview
        preg_match('/<div class=\"govuk\-box\-grey\">(.*)<\/div>/sUi', $contents, $overview_matches);
        if (!empty($overview_matches[1])) {
            $result['sent_overview'] = trim(strip_tags($overview_matches[1]));
        }
        // salary
        preg_match('/<div class=\"sixty\">(.*)<\/div>/sUi', $contents, $salary_cont_matches);
        if (!empty($salary_cont_matches[1])) {
            preg_match('/<span class=\"bold\">(.*)<\/span>/sUi', $salary_cont_matches[1], $salary_lh_matches);
            if (!empty($salary_lh_matches[1])) {
                $salary = explode(' to ', $salary_lh_matches[1]);
                if (isset($salary[0], $salary[1])) {
                    $result['salary_low']  = str_replace(['&#163;', ','], ['', ''], $salary[0]);
                    $result['salary_high'] = str_replace(['&#163;', ','], ['', ''], $salary[1]);
                }
            }
        }
        // working hours
        preg_match('/<div class=\"forty\">(.*)<\/div>/sUi', $contents, $wh_cont_matches);
        if (!empty($wh_cont_matches[1])) {
            preg_match('/<span class=\"bold\">(.*)<\/span>/sUi', $wh_cont_matches[1], $wh_lh_matches);
            if (!empty($wh_lh_matches[1])) {
                $wh = explode(' to ', $wh_lh_matches[1]);
                if (isset($wh[0], $wh[1])) {
                    $result['work_hours_low']  = $wh[0];
                    $result['work_hours_high'] = $wh[1];
                }
            }
        }
        // entry requirements
        $result['entry']      = $this->parseProfileSection($contents, 'entry-requirements');
        $result['skills']     = $this->parseProfileSection($contents, 'skills-required');
        $result['what_do']    = $this->parseProfileSection($contents, 'what-youll-do');
        $result['salary']     = $this->parseProfileSection($contents, 'salary');
        $result['work_hours'] = $this->parseProfileSection($contents, 'working-hours-patterns-and-environment');
        $result['career']     = $this->parseProfileSection($contents, 'career-path-and-progression');
        return $result;
    }

    protected function parseProfileSection(&$contents, $section_id) {
        preg_match('/<section id=\"' . preg_quote($section_id) . '\">(.*)<\/section>/sUi', $contents, $matches);
        if (!empty($matches[1])) {
            // remove a header
            $section = preg_replace('/<h2 class=\"heading\-large\">(.*)<\/h2>/sUi', '', $matches[1]);
            $section = strip_tags($section, 'a');
            $section = str_replace(array("\r", "\n"), ' ', $section);
            $section = preg_replace('/[\t\s]+/', ' ', $section);
            $section = trim($section);
            $section = html_entity_decode($section, ENT_QUOTES, 'utf-8');
            return $section;
        }
        return NULL;
    }

    protected function getContents($url, array $options = array()) {
        $ch = curl_init($url);
        curl_setopt_array($ch, $options + array(
            CURLOPT_RETURNTRANSFER=>true,     // return web page
            CURLOPT_HEADER        =>false,    // do not return headers
            CURLOPT_FOLLOWLOCATION=>true,     // follow redirects
            CURLOPT_ENCODING      =>'',       // handle all encodings
            CURLOPT_USERAGENT     =>'spider', // who am i
            CURLOPT_AUTOREFERER   =>true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT=>9999,     // timeout on connect
            CURLOPT_TIMEOUT       =>30,       // timeout on response
            CURLOPT_MAXREDIRS     =>5,        // stop after redirects
        ));
        $result = curl_exec($ch);
        $err_no = curl_errno($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($err_no || ($http_code !== 200)) {
            $result = false;
        }
        curl_close($ch);
        return $result;
    }
}